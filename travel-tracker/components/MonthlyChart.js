import { Bar } from 'react-chartjs-2';

// This component will expect two props:
    // figuresArray contains the monthly figures to be displayed in the char
    // label is the label or header to be shown at the thop of the chart
export default function MonthlyChart ({ figuresArray, label }){

    const data = {
        labels: [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ],
        datasets: [
            {
                label: label,
                backgroundColor: 'rgba(255, 19, 132, 0.2)',
                borderColor: 'rgba(255, 19, 132, 1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(255, 19, 132, 0.4)',
                hoverBorderColor: 'rgba(255, 19, 132, 1)',
                data: figuresArray
            }
        ]
    }
    return (
        // prop name= { actual value }
        <Bar data={ data } />
    )
}