// 1. Create a "history.js" page to render a table showing the travel history of the logged in user.
import React, { useState, useEffect, useRef } from 'react';
// 4. Import the Head, Table, Alert, Row, Col components and import moment for later use.
import Head from 'next/head';
import { Table, Alert, Row, Col } from 'react-bootstrap';
import moment from 'moment';
// 7. Import the mapboxgl package and add the Mapbox access token.
import mapboxgl from 'mapbox-gl';
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY;

export default function History() {

    const mapContainerRef = useRef(null);

    // 2. Create a records state to store the travel details of the user.
    const [records, setRecords] = useState([]);
    const [longitude, setLongitude] = useState(0);
    // 9. Create states that will store the longitude and the latitude of the origin and destination points
    const [latitude, setLatitude] = useState(0);

    // 11. Create a function "setCoordinates" that will set the coordinates retrieved from the user data to the user states.
    function setCoordinates(long, lat) {
        setLongitude(long);
        setLatitude(lat)
    }

    // 3. Create a "useEffect()" hook to retrieve the details of the user from the database and store the travels in the records state.
    useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data._id) {
                    setRecords(data.travels)
                } else {
                    setRecords([])
                }
            })
    }, [])

    // 10. Create a "useEffect()" hook that will render the map.
    useEffect(() => {
        // 6. Render a map and create a marker centered on the coordinates when the user’s travel record's "origin" or "destination" table elements are clicked.
        const map = new mapboxgl.Map({
            container: mapContainerRef.current,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [longitude, latitude],
            zoom: 12
        })

        const marker = new mapboxgl.Marker()
            .setLngLat([longitude, latitude])
            .addTo(map)

        map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')

        return () => map.remove()

    }, [longitude, latitude])

    // 5. Create a table that will display the travel details of the user. If no record is created show an alert component instead. -> Line 75 to 106
    // 8. Create a "div" container where the map will be rendered -> Line 109
    // 12. Add the function that sets the coordinates when the table data elements for the "origin" and the "destination" are clicked. -> Line 89 && Line 92

    return (
        <React.Fragment>
            <Head>
                <title>My Travel Records</title>
            </Head>
            <Row>
                <Col xs={12} lg={12}>
                    {records.length > 0
                        ?
                        <Table striped bordered hover>
                            <thead>
                                <th>Origin</th>
                                <th>Destination</th>
                                <th>Date</th>
                                <th>Distance (m)</th>
                                <th>Duration (mins)</th>
                                <th>Amount</th>
                            </thead>
                            <tbody>
                                {records.map(record => {
                                    return (
                                        <tr key={record._id}>
                                            <td onClick={() => setCoordinates(record.origin.longitude, record.origin.latitude)}>
                                                {record.origin.longitude}, {record.origin.latitude}
                                            </td>
                                            <td onClick={() => setCoordinates(record.destination.longitude, record.destination.latitude)}>
                                                {record.destination.longitude}, {record.destination.latitude}
                                            </td>
                                            <td>{moment(record.date).format('MMMM DD YYYY')}</td>
                                            <td>{Math.round(record.distance)}</td>
                                            <td>{Math.round(record.duration / 60)}</td>
                                            <td>{record.order.amount}</td>
                                        </tr>
                                    )
                                })
                                }
                            </tbody>
                        </Table>
                        :
                        <Alert variant="info">You have no travel records yet.</Alert>
                    }
                </Col>
                <Col xs={12} lg={12}>
                    <div className='mapContainer' ref={mapContainerRef}></div>
                </Col>
            </Row>
        </React.Fragment>
    )
}