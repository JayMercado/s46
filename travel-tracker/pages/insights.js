import { useEffect, useState } from 'react';
import moment from 'moment';
import { Tabs, Tab } from 'react-bootstrap';
import MonthlyChart from '../components/MonthlyChart';

export default function Insights() {

    // State for storing the distances per month
    const [distances, setDistances] = useState([]);
    const [durations, setDurations] = useState([]);
    const [amounts, setAmounts] = useState([]);
    
    console.log(distances); 

    // Get user details when the components mounts
    useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res=>res.json())
        .then(data=>{

            // Display the data structure of user's travel records
            console.log(data.travels);

            if(data.travels.length > 0 ){
                let monthlyDistance = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                let monthlyDuration = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                let monthlyAmount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

                data.travels.forEach(travel => {
                    
                    // Example January = 0, August = 7
                    const index = moment(travel.date).month();

                    monthlyDistance[index] += (travel.distance/1000);
                    {travel.order
                        ?
                            monthlyAmount[index] += travel.order.amount
                        :
                            monthlyAmount[index] += 0
                    }
                    monthlyDuration[index] += (parseInt(travel.duration)/60);
                });
                setDistances(monthlyDistance);
                setAmounts(monthlyAmount);
                setDurations(monthlyDuration);
            }  
        })

    }, []);

    return (
        <Tabs defaultActiveKey="distances" id="monthlyFigures">
            <Tab eventKey="distances" title="Monthly Distance Traveled">
                <MonthlyChart
                    figuresArray={distances}
                    label="Monthly total in kilometers"
                />
            </Tab>
            <Tab eventKey="amounts" title="Monthly Travel Expenditures">
                <MonthlyChart
                    figuresArray={amounts}
                    label="Monthly total in Philippine Peso"
                />
            </Tab>
            <Tab eventKey="durations" title="Monthly Time Spent Travelling">
                <MonthlyChart
                    figuresArray={durations}
                    label="Monthly total in minutes"
                />
            </Tab>
        </Tabs>
    )
}